const delay = async (ms: number) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(true);
    }, ms);
  });
};

const getDate = async () => {
  await delay(1000);

  const date = new Date();

  return date.toLocaleTimeString();
};

export const revalidate = 5;

export default async function Page() {
  const date = await getDate();

  return (
    <main>
      <div>{date}</div>

      <div>export const revalidate = 5;</div>

      <div>
        <p>open the page, wait at least 5 seconds, refresh.</p>

        <p>desired behavior: the time should be updated</p>

        <p>
          actual behavior: the time is not updated, only after the next refresh
          it is
        </p>
      </div>
    </main>
  );
}
