const getDate = () => {
  const date = new Date();

  return date.toLocaleTimeString();
};

export const revalidate = 5;

export default function Home() {
  const date = getDate();

  return (
    <main>
      <div>{date}</div>

      <div>export const revalidate = 5;</div>

      <div>
        <p>
        open the page, wait at least 5 seconds, refresh. 
        </p>

        <p>
          desired behavior: the time should be updated
        </p>

        <p>
          actual behavior: the time is not updated, only after the next refresh it is
        </p>
      </div>
    </main>
  );
}
