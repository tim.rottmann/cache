const delay = async (ms: number) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(true);
    }, ms);
  });
};

const getDate = async () => {
  await delay(1000);

  const date = new Date();

  return date.toLocaleTimeString();
};

export const revalidate = 0;

export default async function Page() {
  const date = await getDate();

  return (
    <main>
      <div>{date}</div>

      <div>export const revalidate = 0;</div>

      <div>
        <p>this page will always show loading and be SSR dynamically</p>
      </div>
    </main>
  );
}
