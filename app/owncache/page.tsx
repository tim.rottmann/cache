import { inc } from "../util";

export const revalidate = 0;

export default async function Page() {
  const counter = inc();

  return (
    <main>
      <div>{counter}</div>
    </main>
  );
}
