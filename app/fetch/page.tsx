const getDate = async () => {
  const res = await fetch(
    "http://worldtimeapi.org/api/timezone/Europe/Berlin",
    {
      next: { revalidate: 5 },
    }
  );

  return res.json();
};

export default async function Page() {
  const date = await getDate();

  return (
    <main>
      <div>{date.utc_datetime}</div>

      <div>
        fetch, using next: revalidate: 5
      </div>

      <div>must refresh twice :/</div>
    </main>
  );
}
